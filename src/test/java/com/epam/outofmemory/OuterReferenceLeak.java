package com.epam.outofmemory;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertTrue;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@SuppressWarnings("all")
class OuterReferenceLeak {

    @Test
    @Order(0)
    void memoryTest() {
        assertTrue(Runtime.getRuntime().totalMemory() <= 33_554_432);
    }

    @Test
    @Order(1)
    void shouldNotFail() {
        IntStream.range(0, 1000_000)
                .mapToObj(i -> new Class(String.valueOf(i)).getLambdaWithoutReference())
                .collect(Collectors.toList());
    }

    @Test
    @Order(2)
    void shouldFail() {
        IntStream.range(0, 1000_000)
                .mapToObj(i -> new Class(String.valueOf(i)).getLambdaWithReference())
                .collect(Collectors.toList());
    }

    private static class Class {
        private final String outerString;

        Class(String outerString) {
            this.outerString = outerString;
        }

        public Function<String, String> getLambdaWithReference() {
            return s -> outerString + " " + s;
        }

        public Function<String, String> getLambdaWithoutReference() {
            return s -> s + " " + s;
        }
    }
}
