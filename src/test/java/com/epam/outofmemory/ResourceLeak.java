package com.epam.outofmemory;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringJUnitConfig
@Sql("classpath:init-db.sql")
@TestMethodOrder(OrderAnnotation.class)
class ResourceLeak {
    private final static String SQL_INSERT = "INSERT INTO test VALUES(?, ?)";

    @Autowired
    private DataSource dataSource;

    @Test
    @Order(0)
    void memoryTest() {
        assertTrue(Runtime.getRuntime().totalMemory() <= 33_554_432);
    }

    @Test
    @Order(1)
    void shouldNotFail() {
        assertDoesNotThrow(this::closedStatementInsert);
    }

    @Test
    @Order(2)
    void shouldThrowOutOfMemoryError() {
        assertThrows(OutOfMemoryError.class, this::unclosedStatementInsert);
    }

    private void closedStatementInsert() throws SQLException {
        Connection connection = dataSource.getConnection();
        for (int i = 0; i < 100_000; i++) {
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT);
            preparedStatement.setInt(1, i);
            preparedStatement.setString(2, "SOME VALUE " + i);
            preparedStatement.execute();
            preparedStatement.close();
        }
        connection.close();
    }

    private void unclosedStatementInsert() throws SQLException {
            Connection connection = dataSource.getConnection();
            // Fail to be guaranteed and not to wait too much, tests should run with -Xmx32M
            for (int i = 0; i < 100_000; i++) {
                PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT);
                preparedStatement.setInt(1, i);
                preparedStatement.setString(2, "SOME VALUE " + i);
                preparedStatement.execute();
            }
            connection.close();
    }

    @Configuration
    public static class Config {

        @Bean
        DataSource dataSource() {
            HikariConfig hikariConfig = new HikariDataSource();
            hikariConfig.setUsername("postgres");
            hikariConfig.setPassword("postgres");
            hikariConfig.setJdbcUrl("jdbc:postgresql://localhost:5432/out-of-memory");

            return new HikariDataSource(hikariConfig);
        }
    }
}
