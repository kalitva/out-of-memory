package com.epam.outofmemory;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertTrue;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@SuppressWarnings("all")
class ImproperEqualsAndHascodeLeak {
    private static final Map<BadDto, Integer> badMap = new HashMap<>();
    private static final Map<GoodDto, Integer> goodMap = new HashMap<>();

    @Test
    @Order(0)
    void memoryTest() {
        assertTrue(Runtime.getRuntime().totalMemory() <= 33_554_432);
    }

    @Test
    @Order(1)
    void shouldNotFail() {
        for (int i = 0; i < 1000_000; i++) {
            goodMap.put(new GoodDto(1), i);
        }
    }

    @Test
    @Order(2)
    void shouldFail() {
        for (int i = 0; i < 1000_000; i++) {
            badMap.put(new BadDto(1), i);
        }
    }

    private static class BadDto {
        private final int id;

        private BadDto(int id) {
            this.id = id;
        }
    }

    private static class GoodDto {
        private final int id;

        private GoodDto(int id) {
            this.id = id;
        }

        @Override
        public int hashCode() {
            return Objects.hash(id);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            GoodDto goodDto = (GoodDto) o;

            return id == goodDto.id;
        }
    }
}
